#ifndef STATETABLE_H
#define STATETABLE_H


struct statetable {
	int count;
	struct statematrix {
		int next[256];
		bool exclusive;
	} transits[];
};


#endif
