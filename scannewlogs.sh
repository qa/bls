#!/bin/bash
# Copyright 2011,2012,2013 Bernhard R. Link <brlink@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# SOFTWARE IN THE PUBLIC INTEREST, INC. BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# version of the checks being run
checkversion=14
# 14: blhc 0.5
# 13: add E-march-native
# 12: blhc 0.4 + d569ff
# 11: ^(.*/)?buildd-[^/]*/ removed from tagdata
# 10: blhc 0.3 + 23020a
# 9: blhc 0.3
# 8: add line numbers
# 7: add blhc (0.2+)
# 6: add E-array-bounds, W-no-pkgconfig
# 5: add X-test-unexpected-fail

set -o pipefail
set -e

BUILDLOGDIR="/srv/buildd.debian.org/db"
VARDIR="var"
RESCANOLDLIMIT=250
DIST=sid
if ! test -e settings.conf ; then
	echo "Error: No settings.conf in current directory!" >&2
	exit 1
fi
if test -n "$(sed -e "/^[A-Z]*='.*'$/d" -e "/^#/d" settings.conf)" ; then
	echo "Error: Malformed lines in settings.conf lines (must be NAME='value'):" >&2
	sed -e "/^[A-Z]*='.*'$/d" -e "/^#/d" settings.conf >&2
	exit 1
fi
. settings.conf

if [[ ! -f "precompiled.rules" ]] ; then
	echo "Missing precompiled.rules file!" >&2
	exit 1
fi

scanlog() {
	rm -f "$VARDIR"/tocheck.log  || return 1
	r=0
	bunzip2 -c "$1" > "$VARDIR"/tocheck.log && {
		scan/builocheck-scan -r precompiled.rules "$VARDIR"/tocheck.log || r="$?"
		scan/blhc --buildd --arch "$2" "$VARDIR"/tocheck.log || r="$?"
	}
	rm "$VARDIR"/tocheck.log || return 1
	return "$r"
}

checknew=true

if [[ "$#" -eq 0 ]] ; then
	:
elif [[ "$#" -gt 1 ]] ; then
	echo "Unexpected argument count !" >&2
	exit 1
elif [[ "$1" == "--fast" ]] ; then
	checknew=false
else
	echo "Unexpected argument!" >&2
	exit 1
fi

echo "generating new available information"
psql -o "$VARDIR"/currentavail -t -A -F'|' 'service=wanna-build' --command "SELECT DISTINCT package, CASE WHEN binary_nmu_version IS NULL THEN version ELSE version || '+b' || CAST( binary_nmu_version AS TEXT) END AS realversion, architecture, (SELECT EXTRACT(epoch FROM a.timestamp) FROM public.log as a WHERE a.package = t.package AND a.distribution = '$DIST' AND a.version = CASE WHEN t.binary_nmu_version IS NULL THEN t.version ELSE t.version || '+b' || CAST( t.binary_nmu_version AS TEXT) END AND a.architecture = t.architecture  ORDER BY a.timestamp DESC LIMIT 1) as ts FROM packages_public as t WHERE t.distribution = '$DIST' AND t.state IN ('Uploaded', 'Installed', 'Built') ORDER BY t.package, t.architecture;"
sed -e '/|$/d' -i "$VARDIR"/currentavail

echo "import new available information and looking for things to do"
# TODO: use temporary table for available
cat > "${VARDIR}/commands" << EOF
DELETE FROM data.logs_${DIST} WHERE checkdone < ${checkversion} AND package in (SELECT DISTINCT package from data.logs_${DIST} WHERE checkdone < ${checkversion} ORDER BY package LIMIT ${RESCANOLDLIMIT});
CREATE TEMPORARY TABLE available_${DIST} ( package text, version text, architecture text, stamp text);
\COPY available_${DIST} FROM '${VARDIR}/currentavail' WITH DELIMITER '|'
DELETE FROM data.logs_${DIST} AS logs WHERE NOT EXISTS (SELECT * FROM available_${DIST} AS available  WHERE available.package = logs.package AND available.version = logs.version AND available.architecture = logs.architecture AND available.stamp = logs.stamp );
DELETE FROM data.tags_${DIST} AS tags WHERE NOT EXISTS (SELECT * FROM data.logs_${DIST} AS logs WHERE logs.package = tags.package AND logs.version = tags.version AND logs.architecture = tags.architecture AND logs.stamp = tags.stamp );
\COPY (SELECT package, version, architecture, stamp FROM available_${DIST} AS available WHERE NOT EXISTS (select checkdone FROM data.logs_${DIST} AS logs WHERE logs.package = available.package AND available.version = logs.version AND available.architecture = logs.architecture AND available.stamp = logs.stamp)) TO '${VARDIR}/todo.tmpdat' WITH DELIMITER '|'
EOF
: > "${VARDIR}/todo.tmpdat"
psql 'service=qa-buildlogchecks' -f "${VARDIR}/commands" 

echo "importing new data"
while IFS='|' read package version arch stamp lastversion ; do
	pdir="${package:0:1}/${package}"
	echo "processing "$BUILDLOGDIR/$pdir/$version/${arch}_${stamp}_log.bz2""
	scanlog "$BUILDLOGDIR/$pdir/$version/${arch}_${stamp}_log.bz2" "${arch}" | \
		sed -n -e 's/^\([^|]*\)|\([^|]*\)|\([0-9]*\)$/'"$package|$version|$arch|$stamp"'|\1|\2|\3/p' \
		       -e 's/^\([^|]*\)|\([^|]*\)|\?$/'"$package|$version|$arch|$stamp"'|\1|\2|0/p' \
		       >&4 || continue
	printf '%s|%s|%s|%s|%d\n' "$package" "$version" "$arch" "$stamp" "$checkversion" >&3
done < "${VARDIR}/todo.tmpdat" 4> "$VARDIR/"newtags.tmpdat 3> "$VARDIR/"newlogs.tmpdat

# grr, why does it not support multiple \COPY in one command...
psql 'service=qa-buildlogchecks' << EOF
\COPY data.tags_${DIST} FROM '${VARDIR}/newtags.tmpdat' WITH NULL '' DELIMITER '|'
\COPY data.logs_${DIST} FROM '${VARDIR}/newlogs.tmpdat' WITH NULL '' DELIMITER '|'
EOF
rm -r "$VARDIR/"newtags.tmpdat "$VARDIR/"newlogs.tmpdat
